import sys
sys.path.append('../dependencies')

import os
import matplotlib.pyplot as plt
import numpy as np
import ImageFile

from zipfile import ZipFile
from hdf5_read import get_hdf5_data

from pymic.zvi import getcount,zviread

data_path = '../data'
hdf5_filename = 'test_rev.hdf5'
frames = 'seq0_extract.zip'


def parse_imagedata(data):
    """parse compressed image data
    returns a numpy array
    """
    p = ImageFile.Parser()
    p.feed(data)
    im = p.close()
    return np.asarray(im)


# import HDF5 data

feat,data = get_hdf5_data(os.path.join(data_path,hdf5_filename),fields=['center','halo'])
print feat
n_track = len(data)
print n_track


zf = ZipFile(os.path.join(data_path,frames),'r')

# import and display images from zipped file

for frame,fn in enumerate(zf.namelist()):
    ima = parse_imagedata(zf.read(fn,'r'))

    plt.figure()
    plt.imshow(ima)

    for track in range(n_track):
        xg =  data[track]['halo'][frame,:,0]
        yg =  data[track]['halo'][frame,:,1]

        print xg
        print yg

        plt.scatter(xg,yg)

    plt.show()




__author__ = 'olivier'
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import scipy as scp
import numpy as np
import scipy.ndimage.measurements  as measure
from pprint import pprint
from numpy.linalg import norm
import itertools as its
from scipy import ndimage

import networkx as nx

from chain import chain

def lobe_fct(theta0,theta):
    """return lobe function
    rho=1.0 in theta=0.0
    rho=0.0 outside [-theta0,+theta0]
    e.g.

    >>> import matplotlib.pyplot as plt
    >>> import matplotlib.cm as cm
    >>> import numpy as np

    >>> theta = np.linspace(-np.pi,+np.pi,1000)

    >>>for s in np.arange(1.0,5.0):
    >>>    theta0 = np.pi/s
    >>>    rho = lobe_fct(theta0*1.5,theta)
    >>>    plt.polar(theta,rho,label='s=%d'%s)
    >>>    plt.polar([theta0,theta0],[0,1],'k:')
    >>>    plt.polar([-theta0,-theta0],[0,1],'k:')
    >>>plt.legend()
    >>>plt.show()

    """
    s = .5*np.pi/(theta0)
    rho = np.cos(s*theta)**4
    rho[theta<-theta0]=0.0
    rho[theta>theta0]=0.0
    return rho

def lobe_f(theta0,theta):
    s = .5*np.pi/(theta0)
    rho = np.cos(s*theta)**4
    if theta<-theta0:
        rho = 0.0
    if theta>theta0:
        rho=0.0
    return rho

def test_lobe():
    theta = np.linspace(-np.pi, +np.pi, 1000)
    for s in np.arange(1.0, 5.0):
        theta0 = np.pi / s
        print theta0
        rho = lobe_fct(theta0, theta)
        #        plt.plot(theta,rho,label='s=%d'%s)
        plt.polar(theta, rho, label='theta_0 = pi/%d' % s)
        plt.polar([theta0, theta0], [0, 1], 'k:')
        plt.polar([-theta0, -theta0], [0, 1], 'k:')
    plt.legend()
    plt.show()

def zero_cross(x):
    idx0 = np.arange(len(x))
    idx1 = np.roll(idx0,-1)
    return (x[idx1]*x[idx0])<=0


def cyclic_derivative(x):
    idx0 = np.arange(len(x))
    idx1 = np.roll(idx0,-1)
    idx2 = np.roll(idx0,+1)
    return x[idx2]-x[idx1]

class Node(object):
    def __init__(self,label,x,y,parent_label,is_object):
        self.x = x
        self.y = y
        self.parent_label = parent_label
        self.parent = None
        self.children = []
        self.label = label
        self.is_object = is_object

def find_nodes(node_tree):
    """returns all nodes from a tree
    """
    if node_tree.children == []:
        return [node_tree]
    r = [node_tree]
    for c in node_tree.children:
        r.extend(find_nodes(c))
    return r

def hierarchic_labelling(im):
    """compute object(>0 pixels) labelling, hole and inclusions are detected
    return contour list"""
    label_objects = measure.label(im)
    label_holes = measure.label(255-im)
    n,m = im.shape
    objects = measure.find_objects(label_objects[0])
    holes = measure.find_objects(label_holes[0])
    idx_valid_objects = range(1,label_objects[1]+1)
    #reject bg obj touching the image border
    idx_valid_holes = []
    for i,obj in enumerate(holes):
        slice_y,slice_x = obj
        if (slice_x.start==0) | (slice_x.stop == m)\
            |(slice_y.start==0) | (slice_y.stop == n):
            pass
        else:
            idx_valid_holes.append(i+1)

    #build contours
    extern_contours = chain(label_objects[0],idx_valid_objects)
    intern_contours = chain(label_holes[0],idx_valid_holes)

    #build tree
    plt.figure(4)
    plt.imshow(label_holes[0],interpolation='nearest')
    plt.colorbar()

    nodes = {}
    #contour graph get the object contour structure (object containing holes)
    c_graph = nx.Graph()
    for cont in extern_contours:
        label,n,x,y = cont
        sx = x[0]-2
        sy = y[0]-1
        plt.plot(x-1,y-1)
        plt.plot(sx,sy,'o')
        plt.text(x[0],y[0],'obj%d'%label)
        neib_label = label_holes[0][sy,sx]
        nodes['obj%d'%label] = Node(label,x,y,'bg%d'%neib_label,True)
        contour_attr = {'x':x,'y':y,'parent':'bg%d'%neib_label,'is_object':True}
        c_graph.add_node(label,contour_attr)

    for cont in intern_contours:
        label,n,x,y = cont
        sx = x[0]-2
        sy = y[0]-1
        plt.plot(x-1,y-1)
        plt.plot(sx,sy,'o')
        plt.text(x[0],y[0],'bg%d'%label)
        neib_label = label_objects[0][sy,sx]
        nodes['bg%d'%label] = Node(label,x,y,'obj%d'%neib_label,False)
        contour_attr = {'x':x,'y':y,'parent':'bg%d'%neib_label,'is_object':False}
        c_graph.add_node(-label,contour_attr)
        c_graph.add_edge(-label,neib_label)

    plt.figure(14)
    nx.draw(c_graph)
    #extract connected component from the graph (i.e. object with its internal holes)
    connected_components = nx.connected_component_subgraphs(c_graph)
    

    #build tree
    roots = []
    for k in nodes:
        #if has parent,
        n = nodes[k]
        if n.parent_label in nodes:
            nodes[k].parent = nodes[n.parent_label]
            nodes[n.parent_label].children.append(nodes[k])
        else:
            roots.append(nodes[k])

    plt.figure(5)
    plt.title('roots')
    for r in roots:
        for n in find_nodes(r):
            plt.plot(n.x,n.y)

    #build connected parts
    connect_parts = []
    for k in nodes:
        n = nodes[k]
        if n.is_object:
            part = [n]
            for c in n.children:
                part.append(c)
            connect_parts.append(part)

    plt.figure(6)
    plt.title('connected parts')
    for k in connect_parts:
        x = k[0].x
        y = k[0].y
        for c in k[1:]:
            x = np.hstack((x,c.x))
            y = np.hstack((y,c.y))
        plt.plot(x,y)

    for k in connect_parts:
        g = find_cp_graph(k)
        links = compute_connectivity_graph(g,im)

        plt.figure(6)

        for link in links:
            plt.plot([link[0][0],link[1][0]],[link[0][1],link[1][1]],'k',linewidth=3)

        for i,sp in enumerate(g):
            plt.plot(sp.x,sp.y,'or')
            plt.plot(sp.x+sp.v1[0]*10,sp.y+sp.v1[1]*10,'+k')
            plt.plot(sp.x+sp.v2[0]*10,sp.y+sp.v2[1]*10,'+k')
            (x,y)  = sp.lobe()
            plt.plot(x,y,'k')
            dx = 20*sp.dx
            dy = 20*sp.dy
            plt.plot([sp.x,sp.x+dx],[sp.y,sp.y+dy],'k')
            plt.text(sp.x,sp.y,'%d'%i)
    plt.show()

    
def find_corners(w, x, y):
    idx0 = np.array(range(len(x)))
    idx1 = np.roll(idx0, w)
    idx2 = np.roll(idx0, -w)
    v1x = x[idx1] - x[idx0]
    v2x = x[idx2] - x[idx0]
    v1y = y[idx1] - y[idx0]
    v2y = y[idx2] - y[idx0]
    v1 = np.hstack((v1x[:, np.newaxis], v1y[:, np.newaxis]))
    v2 = np.hstack((v2x[:, np.newaxis], v2y[:, np.newaxis]))
    #norm v1,v2
    v1 = norm2d(v1)
    v2 = norm2d(v2)
    v3 = np.cross(v1, v2)

    plt.figure(100)
    plt.plot(v3)

    derv3 = cyclic_derivative(v3)
    zeros = zero_cross(derv3)
    p_in = zeros & (v3 < -0.4)
    p_out = zeros & (v3 > 0.6)
    temp,p_in = find_boolean_clusters(p_in,5)
    temp,p_out = find_boolean_clusters(p_out,5)
    return p_in, p_out, v1,v2,v3

def norm2d(x):
    """returns normalized vectors
    """
    n = np.sqrt(x[:,0]**2+x[:,1]**2)
    return x/n[:,np.newaxis]

class SalientPoint(object):
    def __init__(self,x,y,v1,v2,v3):
        self.x = x
        self.y = y
        self.v1 = v1
        self.v2 = v2
        self.v3 = v3
        self.angle = np.arccos(np.dot(self.v1,self.v2))
        if np.sign(self.v3) < 0:
            self.direction = np.arctan2(-self.v2[1],-self.v2[0])+self.angle/2.0

        else:
            self.direction = np.arctan2(-self.v1[1],-self.v1[0])+self.angle/2.0
        self.dx = np.cos(self.direction)
        self.dy = np.sin(self.direction)
    def lobe(self):
        theta = np.linspace(-np.pi, +np.pi, 30)
        rho = lobe_fct(self.angle, theta)
        x = self.x + 10*rho * np.cos(self.direction+theta)
        y = self.y + 10*rho * np.sin(self.direction+theta)
        return (x,y)

    def distance(self,x,y):
        """returns a connectivity measure to a point depending on direction and distance
        """
        vx = x-self.x
        vy = y-self.y
        dist = norm([vx,vy])
        direction = np.arctan2(vy,vx)
        return dist

    def mutual(self,sp):
        vx = sp.x-self.x
        vy = sp.y-self.y
        dist = norm([vx,vy])
        direction = np.array([vx,vy])/dist
        alpha = np.dot(-direction,np.array([sp.dx,sp.dy]))
        beta = np.dot(direction,np.array([self.dx,self.dy]))

        print (2-min(alpha,beta))*dist,dist,alpha,beta
        linked = (dist<150) & (min(alpha,beta)>.8)
        return linked

def profile(ima,p0,p1,num=None):
    """extract interpolated profile along a segment in an image
    """
    if num is None:
        num = norm([p0[0]-p1[0],p0[1]-p1[1]])
    n = np.linspace(p0[0],p1[0],num)
    m = np.linspace(p0[1],p1[1],num)
    return [n,m,ndimage.map_coordinates(ima, [m,n], order=0)]

def compute_connectivity_graph(sp_list,im):
    """returns a list of connected sp
    connection that touch the bg are discarded"""
    con_list = []
    c = its.combinations(sp_list,2)
    for s1,s2 in c:
        m,n,v =profile(im,(s1.x,s1.y),(s2.x,s2.y))
        if 0 not in v[3:-2]:
            con_list.append(((s1.x,s1.y),(s2.x,s2.y)))
    return con_list

def find_best_cuts(graph):
    """returns a simplified graph
    """

#        linked = s1.mutual(s2)
#        if linked:
#            #check with image (no bg pixels allowed
#            m,n,v =profile(im,(s1.x,s1.y),(s2.x,s2.y))
#            if 0 not in v[3:-2]:
#                con_list.append(((s1.x,s1.y),(s2.x,s2.y)))

    return graph

def find_cp_graph(cp):
    """find corners for a connected part
    """
    w = 8
    #object
    pin,pout,v1,v2,v3 = find_corners(w, cp[0].x, cp[0].y)
    x_corners = cp[0].x[pin]
    y_corners = cp[0].y[pin]
    v1_corners = v1[pin,:]
    v2_corners = v2[pin,:]
    v3_corners = v3[pin]
    g = []

    #holes
    for c in cp[1:]:
        pin,pout,v1,v2,v3 = find_corners(w, c.x, c.y)
        x_corners = np.hstack((x_corners,c.x[pout]))
        y_corners = np.hstack((y_corners,c.y[pout]))
        v1_corners = np.vstack((v1_corners,v1[pout,:]))
        v2_corners = np.vstack((v2_corners,v2[pout,:]))
        v3_corners = np.hstack((v3_corners,v3[pout]))
        
    for x,y,v1,v2,v3 in zip(x_corners,y_corners,v1_corners,v2_corners,v3_corners):
        g.append(SalientPoint(x,y,v1,v2,v3))

    return g

def test_contours():
    im = plt.imread('../../test/data/h_bubble.tif')
    label_obj = measure.label(im)
    label_holes = measure.label(255 - im)
    idx = range(1, label_obj[1] + 1)
    plt.figure(0)
    plt.imshow(label_obj[0], origin='lower', interpolation='nearest')
    plt.colorbar()
    contours = chain(label_obj[0], idx)
    print contours
    w = 10
    for cont in contours:
        i, n, x, y = cont
        plt.figure(1)
        plt.plot(x - 1, y - 1)

        p_in, p_out = find_corners(w, x, y)
        plt.figure(1)
        plt.plot(x[p_in], y[p_in], 'o:')
        plt.plot(x[p_out], y[p_out], 'o:')
    plt.show()

def find_boolean_clusters(x,th=2):
    """groups series of consecutive 1, 0 gap <= th are replaced by 1
    cyclic, find first 0 to 1 transition
    """
    nz = np.nonzero(x)[0]
    try:
        offset = nz[nz>0][0]
        x = np.roll(x,-offset)

        fx = np.ones(x.shape).astype('bool')
        fxc = np.zeros(x.shape).astype('bool')
        pos = 0
        status = True
        icount = 0
        while pos<len(x):
            if status:
                if x[pos]:
                    icount += 1
                else:
                    ocount = 1
                    status = False
            else:
                if x[pos]:
                    if ocount>=th:
                        fx[pos-ocount:pos] = False
                        fxc[pos-icount/2-ocount-1] = True
                        icount = 1
                    else:
                        icount += ocount+1
                    status = True
                else:
                    ocount += 1
            pos += 1
        if ocount>=th:
            fx[pos-ocount:pos] = False
            fxc[pos-icount/2-ocount-1] = True
        fx = np.roll(fx,offset)
        fxc = np.roll(fxc,offset)
        return (fx,fxc)
    except :
        return (x,x)

if __name__ == "__main__":

#    test_lobe()
#    test_contours()

#    im = plt.imread('../../test/data/bubble_chain.tif')
    im = plt.imread('../../test/data/h_bubble.tif')
    hierarchic_labelling(im)
    x = np.array([1,0,1,0,1,1,0,0,1,0,0,0,1,0])
    x = np.array([1,0,0,1,0,1,1,0,0,1,1,1,0,0,0,1,0,0,1,1])
#    x = np.array([0,0])
    print x
#    print group(x,1).astype('int')
    print find_boolean_clusters(x,2)[0].astype('int')
    print find_boolean_clusters(x,2)[1].astype('int')
#    print group(x,3).astype('int')
#    print group(x,4).astype('int')



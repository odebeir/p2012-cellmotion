/*CHAIN.CPP*/

void zero_borders(char * str,char len,int sizex,int sizey,int ext);
int build_chain(int * str,int sizex,int sizey, int label,int *str1,int *str2);
int build_chain4ex(int * str,int sizex,int sizey, int label,int *str1,int *str2);

/*Conventions de directions...*/

int ChainBufM[CHAINMAXLENGTH];
int ChainBufN[CHAINMAXLENGTH];
int Dy[16] = {-1, -1 , 0 ,+1 ,+1, +1, 0 ,-1,-1, -1 , 0 ,+1 ,+1, +1, 0 ,-1};
int Dx[16] = {0, +1, +1, +1, 0, -1, -1, -1, 0, +1, +1, +1, 0, -1, -1, -1};
int	Dx4[8]={ 0,-1,-1,-1, 0, 1, 1, 1};
int	Dy4[8]={ 1, 1, 0,-1,-1,-1, 0, 1};

/*
fonction qui calcule une chaine8 sur l'image pi et sauve le resultat dans 'chain'
retourne une structure Matlab contenant les [x,y] des points de la chaine
de l'objet Label de l'image pi. La recherche est faite e partir du premier pixel == Label
LabelImage est un plan de int
label est un double
 */


void zero_borders(char * str,char len,int sizex,int sizey,int ext) {
	//set borders pixels to 0 to avoid craches
	int *p;
	int i;

	p = ( int *)str;

	for(i=0;i<sizey;i++) {
		*(p+i) = 0;
		*(p+i+sizey) = 0;
		*(p+(sizex-1)*sizey) = 0;
		*(p+(sizex-2)*sizey) = 0;
		if(ext){
			*(p+i+2*sizey) = 0;
			*(p+(sizex-3)*sizey) = 0;
		}
	}
	for(i=0;i<sizex;i++) {
		*(p+i*sizey) = 0;
		*(p+i*sizey+1) = 0;
		*(p+(i+1)*sizey-1) = 0;
		*(p+(i+1)*sizey-2) = 0;
		if(ext){
			*(p+i*sizey+2) = 0;
			*(p+(i+1)*sizey-3) = 0;
		}
	}
}

int build_chain(int * str,int sizex,int sizey, int label,int *str1,int *str2)
{
	int m,n,i,j,done;
	int x,y,x0,y0,x1,y1,prev_dir,new_dir,d;
	long int l;
	int *pint;
	int *X,*Y;

	m = sizex;
	n = sizey;
	pint = ( int*)str;

	x0=-1;
	for(l=0;l<m*n;l++)
	{
		if(*pint==label)
		{
			x0=l/m;
			y0=l%m;
			break;
		}
		pint++;
	}
	if(x0<0)// pas d'objet de ce label
	{
		return 0;
	}

	pint = ( int*)str;

	prev_dir = 3;
	x=x0;
	y=y0;
	i = 0;
	while(1)
	{
		done=0;
		ChainBufM[i] = x+1;
		ChainBufN[i] = y+1;
		i++;
		new_dir = (prev_dir+5)%8;
		for(d = new_dir;d<new_dir+8;d++)
		{
			x1=x+Dx[d];
			y1=y+Dy[d];
			if(*(pint+x1*m+y1)==label)
			{
				x=x1;y=y1;
				prev_dir = d%8;
				done=1;
				break;
			}
		}
		if((i>=CHAINMAXLENGTH)||((x==x0)&&(y==y0))||(done==0)) break;
	}

	X = (int*)str1;
	Y = (int*)str2;
	for(j=0;j<i;j++)
	{
		*(Y+j) = (int)ChainBufM[j];
		*(X+j) = (int)ChainBufN[j];
	}
	return i;

};


//_____________________________________________________________OBJECT
int build_chain4ex(int * str,int sizex,int sizey, int label,int *str1,int *str2)
//fonction qui calcule une chaine8 sur l'image pi et sauve le résultat dans 'chain'
//retourne une structure Matlab contenant les [x,y] des points de la chaine
//de l'objet Label de l'image pi. La recherche est faite à partir du premier pixel == Label
//LabelImage est un plan de double
//label est un double
//le contour construit est extérieur et de connexité 4
{
	int m,n,i,j,dir;
	int x,y,x0,y0;
	long int l;
	int *pint;
	int *X,*Y;

	m = sizex;
	n = sizey;
	pint = (int*)str;

	//recherche du premier pixel
	x0=-1;
	for(l=0;l<m*n;l++)
	{
		if(*pint==label)
		{
			x0=l/m;
			y0=l%m;
			break;
		}
		pint++;
	}
	x0 = x0-1;
	y0 = y0;
	if(x0<0) //pas d'objet de ce label
	{
		return 0;
	}

	pint = ( int*)str;

	dir = 2;
	x = x0;
	y = y0;
	i = 0;
	while(1)
	{
		switch(dir){
		case 0:
			if((*(pint+(x+Dx[2])*m+(y+Dy[2]))==label)){x=x+Dx[4];y=y+Dy[4];dir=4;break;}
			if((*(pint+(x+Dx[1])*m+(y+Dy[1]))==label)){x=x+Dx[2];y=y+Dy[2];dir=2;break;}
			if((*(pint+(x+Dx[0])*m+(y+Dy[0]))==label)){x=x+Dx[2];y=y+Dy[2];dir=2;break;}
			if((*(pint+(x+Dx[6])*m+(y+Dy[6]))==label)){x=x+Dx[0];y=y+Dy[0];dir=0;break;}
			x=x+Dx[6];y=y+Dy[6];dir=6;
			break;
		case 2:
			if((*(pint+(x+Dx[4])*m+(y+Dy[4]))==label)){x=x+Dx[6];y=y+Dy[6];dir=6;break;}
			if((*(pint+(x+Dx[3])*m+(y+Dy[3]))==label)){x=x+Dx[4];y=y+Dy[4];dir=4;break;}
			if((*(pint+(x+Dx[2])*m+(y+Dy[2]))==label)){x=x+Dx[4];y=y+Dy[4];dir=4;break;}
			if((*(pint+(x+Dx[0])*m+(y+Dy[0]))==label)){x=x+Dx[2];y=y+Dy[2];dir=2;break;}
			x=x+Dx[0];y=y+Dy[0];dir=0;
			break;
		case 4:
			if((*(pint+(x+Dx[6])*m+(y+Dy[6]))==label)){x=x+Dx[0];y=y+Dy[0];dir=0;break;}
			if((*(pint+(x+Dx[5])*m+(y+Dy[5]))==label)){x=x+Dx[6];y=y+Dy[6];dir=6;break;}
			if((*(pint+(x+Dx[4])*m+(y+Dy[4]))==label)){x=x+Dx[6];y=y+Dy[6];dir=6;break;}
			if((*(pint+(x+Dx[2])*m+(y+Dy[2]))==label)){x=x+Dx[4];y=y+Dy[4];dir=4;break;}
			x=x+Dx[2];y=y+Dy[2];dir=2;
			break;
		case 6:
			if((*(pint+(x+Dx[0])*m+(y+Dy[0]))==label)){x=x+Dx[2];y=y+Dy[2];dir=2;break;}
			if((*(pint+(x+Dx[7])*m+(y+Dy[7]))==label)){x=x+Dx[0];y=y+Dy[0];dir=0;break;}
			if((*(pint+(x+Dx[6])*m+(y+Dy[6]))==label)){x=x+Dx[0];y=y+Dy[0];dir=0;break;}
			if((*(pint+(x+Dx[4])*m+(y+Dy[4]))==label)){x=x+Dx[6];y=y+Dy[6];dir=6;break;}
			x=x+Dx[4];y=y+Dy[4];dir=4;
			break;
		}
		ChainBufM[i] = x+1;
		ChainBufN[i] = y+1;
		i++;
		if((i>=CHAINMAXLENGTH)||((x==x0)&&(y==y0))) break;
	}

	X = (int*)str1;
	Y = (int*)str2;

	for(j=0;j<i;j++)
	{
		*(Y+j) = (int)ChainBufM[j];
		*(X+j) = (int)ChainBufN[j];
	}
	//return chain;
	return i;

};


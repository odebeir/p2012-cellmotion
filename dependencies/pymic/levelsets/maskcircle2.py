import numpy as npy
from scipy.ndimage.filters import convolve

def maskcircle2(I,type='small'):
    """auto pick a circular mask for image I
    built-in mask creation function
    Input: I   : input image
    type: mask shape keywords
     Output: m  : mask image
     """
    if I.ndim!=2:
        temp = I[:,:,0]
    else:
        temp = I

    h = npy.asarray([[0,1,0],[1,-4,1],[0,1,0]])
    T = convolve(temp,h)
    T[0,:] = 0
    T[-1,:] = 0
    T[:,0] = 0
    T[:,-1] = 0

    thre = npy.max(T)*.5
    idx = npy.abs(T)>thre
    n_idx = npy.sum(idx)

    x,y = npy.meshgrid(range(0,temp.shape[1]),range(0,temp.shape[0]))

    print x.shape,y.shape,T.shape


    cx = npy.sum(x[idx])/n_idx
    cy = npy.sum(y[idx])/n_idx

    m = npy.zeros(temp.shape)
    p,q = temp.shape

    if type=='small':
        r = 10
        n = npy.zeros(x.shape)
        n[((x-cx)**2+(y-cy)**2)<r**2] = 1
        m = n
    elif type=='medium':
        r = min(min(cx,p-cx),min(cy,q-cy))
        r = max(2/3*r,25)
        n = npy.zeros(x.shape)
        n[((x-cx)**2+(y-cy)**2)<r**2] = 1
        m = n
    elif type=='large':
        r = min(min(cx,p-cx),min(cy,q-cy))
        r = max(2/3*r,60)
        n = npy.zeros(x.shape)
        n[((x-cx)**2+(y-cy)**2)<r**2] = 1
        m = n
    elif type=='whole':
        n = npy.zeros(x.shape)
        n[10:-10,10:-10] = 1
        m = n





    return m



def main():
    import matplotlib.pyplot as plt

    I = plt.imread('brain.jpg')[:,:,1]

    m = maskcircle2(I,type='small')
    plt.imshow(m)
    plt.figure()
    m = maskcircle2(I,type='medium')
    plt.imshow(m)
    plt.figure()
    m = maskcircle2(I,type='large')
    plt.imshow(m)
    m = maskcircle2(I,type='whole')
    plt.imshow(m)
    plt.show()


if __name__ == '__main__':
    main()


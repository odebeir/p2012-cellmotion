import numpy as npy

def kappa(I):
    """get curvature information of input image
    input: 2D image I
    output: curvature matrix KG (size+=1)
    """
    m,n = I.shape
    P = npy.hstack((npy.zeros((m,1)),I,npy.zeros((m,1))))
    m,n = P.shape
    P = npy.vstack((npy.zeros((1,n)),P,npy.zeros((1,n))))

    fy = P[2:,1:-1] - P[0:-2,1:-1]
    fx = P[1:-1,2:] - P[1:-1,0:-2]
    fyy = P[2:,1:-1] + P[0:-2,1:-1] - 2 * I
    fxx = P[1:-1,2:] + P[1:-1,0:-2] - 2 * I
    fxy = .25*(P[2:,2:]+P[:-2,:-2]-P[:-2,2:]-P[2:,:-2])
    G = npy.sqrt(fx**2+fy**2)
    K = (fxx*fy**2-2*fxy*fx*fy+fyy*fx**2)/(npy.power(fx**2+fy**2,1.5)+1e-10)
    KG = K*G

    return KG

def main():
    import matplotlib.pyplot as plt

    I = plt.imread('brain.jpg')[:,:,1]

    KG = kappa(I)

    plt.imshow(KG)
    plt.colorbar()
    plt.show()

if __name__ == '__main__':
    main()


# -*- coding: utf-8 -*-
'''
Mean shift implementation using integral images
integral function is extracted from opencv library
'''

__author__ = 'olivier'

import sys
import cv
import numpy as np

import pylab

import matplotlib.patches as mpatches
import matplotlib.lines as mlines

from scipy.ndimage import imread

def test_image(s=0):
    t = np.zeros((100,120),dtype='uint8')
    if s==0:
        t[30:50,20:40] = 255
    if s==1:
        x,y = np.meshgrid(range(20,40),range(30,50))
        t[30:50,20:40] = x
    if s==2:
        x,y = np.meshgrid(range(20,40),range(30,50))
        t[30:50,20:40] = 55 - np.sqrt((x-30)**2+(y-40)**2)
    if s==3:
        t = imread('../../test/data/exp0001crop.jpg').astype(float)
        #normalization
        t = (t-t.min())/(t.max()-t.min())
        t = t**2
    return t

def get_sum(integral_image,x1,y1,x2,y2):
    """returns sum from CVarr integral image
    """
    a = cv.Get2D(integral_image,y1,x1)[0]
    b = cv.Get2D(integral_image,y1,x2)[0]
    c = cv.Get2D(integral_image,y2,x2)[0]
    d = cv.Get2D(integral_image,y2,x1)[0]
    return c+a-b-d

class IntegratedImage(object):
    """Object keeping the integral image and the X,Y integral images (internal opencv)
    """
    def __init__(self,array):
        self.m,self.n = array.shape
        X,Y = np.meshgrid(range(1,self.n+1),range(1,self.m+1))

        self.mat = cv.fromarray(array)
        x_mat = cv.fromarray((array*X).astype(float))
        y_mat = cv.fromarray((array*Y).astype(float))

        self.sum = cv.CreateMat(self.m+1,self.n+1,cv.CV_64F) #!!!!! +1
        self.xsum = cv.CreateMat(self.m+1,self.n+1,cv.CV_64F) #!!!!! +1
        self.ysum = cv.CreateMat(self.m+1,self.n+1,cv.CV_64F) #!!!!! +1

        cv.Integral(self.mat,self.sum)
        cv.Integral(x_mat,self.xsum)
        cv.Integral(y_mat,self.ysum)

    def _valid_rectangle(self,rect):
        """return a valid rectangle w.r.t. image size
        """

        x1,x2 = (min(rect[0],rect[2]),max(rect[0],rect[2]))
        y1,y2 = (min(rect[1],rect[3]),max(rect[1],rect[3]))
        x1 = int(max(0,x1))
        x2 = int(min(self.n,max(x2,0)))
        y1 = int(max(0,y1))
        y2 = int(min(self.m,max(y2,0)))

        return (x1,y1,x2,y2)
    def _get_sum(self,integral_image,rect):
        """returns sum from CVarr integral image
        """
        x1,y1,x2,y2 = self._valid_rectangle(rect)

        a = cv.Get2D(integral_image,y1,x1)[0]
        b = cv.Get2D(integral_image,y1,x2)[0]
        c = cv.Get2D(integral_image,y2,x2)[0]
        d = cv.Get2D(integral_image,y2,x1)[0]
        
        return c+a-b-d

    def _surf_rect(self,rect):
        return (rect[2]-rect[0])*(rect[3]-rect[1])

    def get_value(self,x,y):
        return cv.Get2D(self.mat,int(y),int(x))[0]
    
    def find_g(self,rect):
        """returns centroid of the rectangle
        """
        sum = self._get_sum(self.sum,rect)
        xsum = self._get_sum(self.xsum,rect)
        ysum = self._get_sum(self.ysum,rect)

        if sum>0:
            xg = xsum/sum
            yg = ysum/sum
            mean = sum/self._surf_rect(rect)
        else:
            xg = (rect[0]+rect[2])/2.0
            yg = (rect[1]+rect[3])/2.0
            mean = 0

        return (xg,yg,mean)


def shift(target,x0,y0,w,adapt = 'none'):
    """converge using meanshift
    """
    N = 40
    path = np.zeros((N,2))
    x1 = x0-w/2.0
    y1 = y0-w/2.0
    x2 = x0+w/2.0
    y2 = y0+w/2.0
    path[0,:] = [x0,y0]
    w0 = w
    xg = x0
    yg = y0
    for iter in range(1,N):
        rect = (x1,y1,x2,y2)
        xg,yg,mean = target.find_g(rect)
        mean_c= target.get_value(xg,yg)
        path[iter,:] = [xg,yg]
        d2 = (x1+w/2.0-xg)**2+(y1+w/2.0-yg)**2
        if d2<.1:
            path[iter+1,:] = [xg,yg]
            path = path[0:iter+1,:]
            break
        if adapt is 'none':
            pass
        if adapt is 'iter_dec':
            w = w*.8
        if adapt is 'step_dec':
            w = .5*np.sqrt(d2)
        if adapt is 'intensity':
            w = w0*(1-mean_c)
        if adapt is 'custom':
            weight = [1.0]*5 + [.75]*5 + [.5]*5 + [.25]*(N-15)
            w = w0*weight[iter]
        x1 = xg-w/2.0
        y1 = yg-w/2.0
        x2 = xg+w/2.0
        y2 = yg+w/2.0

    return (xg,yg,path)

def test1():
    """randow test
    """
    im = test_image(3)
    pylab.imshow(im, interpolation='nearest')

    target = IntegratedImage(im)
    m,n = im.shape
    print m,n
    for i in range(10):

        x1 = int(np.random.random()*n)-25
        y1 = int(np.random.random()*m)-25
        x2 = x1+25
        y2 = y1+25

        rect = (x1,y1,x2,y2)
        xg,yg,mean = target.find_g(rect)

        rect = mpatches.Rectangle((x1,y1),x2-x1,y2-y1,fill=False)
        start = mpatches.Circle((xg,yg), 0.5,ec=[1,1,1],fc='none')
        stop = mpatches.Circle(((x2+x1)/2.0,(y1+y2)/2.0), 0.5,fc=[1,1,1],ec='none')

        arrow = mpatches.Arrow((x2+x1)/2.0,(y1+y2)/2.0, xg-(x2+x1)/2.0, yg-(y2+y1)/2.0 , width=0.5, color = 'y')

        pylab.gca().add_patch(rect)
        pylab.gca().add_patch(start)
        pylab.gca().add_patch(stop)
        pylab.gca().add_patch(arrow)

    pylab.colorbar()

def test2():
    """grid test
    """
    im = test_image(3)

    target = IntegratedImage(im)
    m,n = im.shape
    print m,n
    w = 30
    step = 5
    pylab.imshow(im, interpolation='nearest')

    for x in range(0,n,step):
        for y in range(0,m,step):

            x1 = x-w/2
            y1 = y-w/2
            x2 = x+w/2
            y2 = y+w/2

            rect = (x1,y1,x2,y2)
            xg,yg,mean = target.find_g(rect)


            rect = mpatches.Rectangle((x1,y1),x2-x1,y2-y1,fill=False)
            start = mpatches.Circle((xg,yg), 0.5,fc=[1,1,1],ec='none')
            stop = mpatches.Circle(((x2+x1)/2.0,(y1+y2)/2.0), 0.5,ec=[1,1,1],fc='none')

            arrow = mpatches.Arrow((x2+x1)/2.0,(y1+y2)/2.0, xg-(x2+x1)/2.0, yg-(y2+y1)/2.0 , width=0.5, color = 'y')

#            pylab.gca().add_patch(rect)
            pylab.gca().add_patch(start)
            pylab.gca().add_patch(stop)
            pylab.gca().add_patch(arrow)

    pylab.colorbar()

def test3():
    """iterations
    """
    im = test_image(3)

    target = IntegratedImage(im)
    m,n = im.shape
    print m,n
    w = 30
    step = 10

    fig = pylab.figure()
    ax = pylab.axes([0,0,1,1])

    pylab.imshow(im, interpolation='nearest')

    for x in range(0,n,step):
        for y in range(0,m,step):

            xg,yg,path = shift(target,x,y,w,adapt='custom')

            start = mpatches.Circle((x,y), 0.5,fc=[1,1,1],ec='none')
            stop = mpatches.Circle((xg,yg), 0.5,ec=[1,1,1],fc='none')

            arrow = mpatches.Arrow(x,y, xg-x, yg-y , width=0.5, color = 'y')
            line = mlines.Line2D(path[:,0], path[:,1], lw=path.shape[0]*.3, color = 'y')

#            pylab.gca().add_patch(start)
            pylab.gca().add_patch(stop)
            ax.add_line(line)


    pylab.colorbar()

if __name__ == "__main__":
    import doctest
    print cv
    test3()
    pylab.show()



